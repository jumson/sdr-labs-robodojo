# SDR Labs

This repository will be used to enable the SDR labs at the Robo Dojo, located at the Naval Postgraduate School in Monterey California.

The folowing link provides a machine with a configurable environment to allow for execution of the lab:

[![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/gl/jumson%2Fsdr-lab-machines/master?filepath=.%2FTOC_Files%2FTableOfContents.ipynb)

Get the sample analyzer notebook here:
[![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/gl/jumson%2Fsdr-lab-machines/master?filepath=.%2Fsample_analyzer.ipynb)

* you can(should) execute each code section by pressing [Shift]+[Enter]
* This environment will include access to gnuradio 3.8, numpy, scipy, matplotlib, and everything needed to support those on Python3.
* * it also will allow python2, C++, and Javascript notebooks, as well as SoS books (script of scripts)
* 
