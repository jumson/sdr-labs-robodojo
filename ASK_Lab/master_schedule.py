#!/usr/bin/env python3
#https://docs.python.org/3.6/library/sched.html
import sched, time
import subprocess

##schedule a bunch of actions based on absolute times

# the action should be a function that runs a command line, and then checks for exit or early exit, and responds

def sdr_listen(freq,outfile,samp_rate,duration):
    # this command will need to be run: 'sudo ./get_filtered_samples.py -o outfile -s 2e6 -f 315e6 -d duration'
    commands = 'sudo ./get_filtered_samples.py -o ' + outfile + ' -s ' + str(samp_rate) + ' -f ' + str(freq) + ' -d ' + str(duration)
    args = commands.split()
    proc = subprocess.run(args)
    start = time.time()

    while not proc.returncode:
        pass
    # if it runs shorter than half the duration, recurse
    if time.time() - start < duration/2:
        sdr_listen(freq,outfile,samp_rate,duration)
    print('process returned!',proc.returncode)



## Timer / scheduler stuff goes here
# scheduler.enterabs(time, priority, action, argument=(), kwargs={})
s = sched.scheduler(time.time, time.sleep)

# set a base time -- such as 00:00 at 29 October 2018
# time_29_10_2018 = 1540796400.5318391
# and iterate off of that to produce absolute values
# GMT?
time_29_10_2018 = time_base =  1540796400.5318391
hours_24 = 86400
hours_12 = hours_24/2
hours_6 = hours_12/2
hours_3 = hours_6/2
hour = hours_3/3    #3600
minute = hour/60    #60



#time to start recording
start_rec = time.time()  #time_base+hours_12

# how many times? 10 seconds on, 10 seconds off, == three times a minute for an hour
for times in range(60):
    
    samp_rate = 2e6
    duration = 10

    outfile1 = 'gdo_'+str(start_rec+(10+(times*minute)))+'.raw'

    s.enterabs(start_rec+(10+(times*minute)), 1, sdr_listen, argument=('315018e3',outfile1,samp_rate,duration))

    outfile2 = 'solid_'+str(start_rec+(10+(times*minute)))+'.raw'
    s.enterabs(start_rec+(30*(times*minute)), 2, sdr_listen, argument=('433943e3',outfile2,samp_rate,duration))

    outfile3 = 'doorbell_'+str(start_rec+(10+(times*minute)))+'.raw'
    s.enterabs(start_rec+(50*(times*minute)), 3, sdr_listen, argument=('314999e3',outfile3,samp_rate,duration))

s.run()