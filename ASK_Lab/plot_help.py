#!/usr/bin/env python
import matplotlib.pyplot as plt

def plot_spec(data,samp_rate,cent_freq,name='',save_pic=False):
    plt.figure(figsize=(20,4))
    sig_name = name+"_spectragram_"+str(cent_freq)+"Hz_Samp Rate_"+str(samp_rate)
    plt.specgram(data, NFFT=256, Fs=samp_rate, Fc=cent_freq)
    plt.title(sig_name)
    plt.xlabel("Time")
    plt.ylabel("Frequency")
    plt.style.use('seaborn-poster')
    #uncomment below to save figure as .jpg, pdf, etc
    if save_pic:
        plt.savefig(sig_name+".jpg", bbox_inches='tight', pad_inches=0.5)
    plt.show()



def plot_psd(data,samp_rate,cent_freq,name='',save_pic=False):
    # now the sample is smaller, and it's Power Specrat Density looks like this
    sig_name = name+"_psd_"+str(cent_freq)+"Hz_Samp Rate_"+str(samp_rate)
    plt.figure(figsize=(20,4))
    plt.title("PSD of "+sig_name)
    plt.psd(data, NFFT=256, Fs=samp_rate, Fc=cent_freq, color="green")
    if save_pic:
        plt.savefig(sig_name+".jpg", bbox_inches='tight', pad_inches=0.5)
    plt.show()
    

def plot_slice(dat,save_pic=False):
    signal_name = 'The_Sliced_Signal'
    # the x axis will be each value --
    x = range(len(dat))
    # the y axis is dat 
    plt.figure(figsize=(20,5))
    #plt.scatter(x,dat_sliced[lefty:righty])
    plt.plot(x,dat)
    plt.title(signal_name)
    if save_pic:
        plt.savefig(signal_name+".jpg", bbox_inches='tight', pad_inches=0.5)
    plt.show()