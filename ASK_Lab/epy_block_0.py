#!/opt/conda/bin python
# 
##################################################
# Some functions I use frequently
# 
# Author: Jon Munson
# Description: 
# 
##################################################
# example from https://www.geeksforgeeks.org/writing-files-background-python/
# Inherting the base class 'Thread'
import threading
class AsyncWrite(threading.Thread): 
 
    def __init__(self, out):
 
        # calling superclass init
        threading.Thread.__init__(self) 
        self.text = ''
        self.out = out
 
    def log(self, text):
        self.text = text
        f = open(self.out, "a+")
        f.write(self.text)
        f.close()
 
        #print("Finished background file write to", self.out)
        # usage:     background = AsyncWrite('stuff to write\n', filename)
                #    background.start()
        # usage:     l = AsyncWrite(filename)
                #    l.log('log this \n')

"""
Embedded Python Blocks:

Each time this file is saved, GRC will instantiate the first class it finds
to get ports and parameters of your block. The arguments to __init__  will
be the parameters. All of them are required to have default values!
"""

import numpy as np
from gnuradio import gr


# class blk(gr.sync_block):  # other base classes are basic_block, decim_block, interp_block
class blk(gr.basic_block):
    """Embedded Python Block example - a simple multiply const"""

    def __init__(self, working_samp_rate=400e3):  # only default arguments here
        """arguments to this function show up as parameters in GRC"""
        gr.basic_block.__init__(
            self,
            name='print symbols',   # will show up in GRC
            in_sig=[np.complex64],
            out_sig=[]
        )
        # if an attribute with the same name as a parameter is found,
        # a callback is registered (properties work, too).
        self.working_samp_rate = working_samp_rate
        # self.filename = 'D:\OneDrives\OneDrive - Naval Postgraduate School\SDRLabs\sdr-labs-robodojo\ASK_Lab\data.out'
        self.sample_holder = np.array([],dtype='complex64')
        # self.slicer = Slices(samp_rate=working_samp_rate)
        self.logfile = 'D:\OneDrives\OneDrive - Naval Postgraduate School\SDRLabs\sdr-labs-robodojo\ASK_Lab\\flowgraph_data.log'
        self.l = AsyncWrite(self.logfile)
        # experimentally determined value for genie  garage door opener
        # used to seperate bursts
        #self.dist_between_bursts = .03*working_samp_rate
        self.dist_between_bursts = .01*working_samp_rate

    # def work(self, input_items, output_items):
        # in a sync block, this was "work()", but the basic_block requires "general work"
    def general_work(self, input_items, output_items):
        """example: multiply with constant"""
        #output_items[0][:] = input_items[0] * self.example_param
        self.sample_holder = np.append(self.sample_holder,input_items[0])
        # would this be one second of data?
        if self.sample_holder.size > self.working_samp_rate*8:
            gathered_log = ('\n \t -------start check ----------------\n')
            gathered_log = gathered_log + ('checking burst with length '+ str(self.sample_holder.size))
            #self.l('checking with length '+ str(self.sample_holder.size))
            this_sample = np.array(self.sample_holder)

            # the complex data is converted into magnitudes
            mags = np.absolute(this_sample)

            # the threshold is automatically calculated by finding the maximum magnitude, then dividing it by two
            # this assumes no "noise" was included directly in this filtered data
            mythresh = np.amax(mags)/2
            mythresh = .001
            
            # numpy creates an array of positions where the magnitudes are higher than the threshold value
            # this accomplishes nearly the same thing as gnuradio binary slicer, a kind of metadata though
            sliced_data = np.array(np.where(mags > mythresh )[0])
            
            # the differences between each consecutive number is calculated and placed in this new array
            new = np.array(np.ediff1d(sliced_data))
            gathered_log = gathered_log + ('\n \t initial sliced positions of past threshold values (new) \n') + str(new.size)

            # if the differences between the sliced data postions are greater than the average distance between the normal bursts,
            # then the number indicates a new burst, and this array holds the starting location of each noew burst.
            burst_segments = np.where(new > self.dist_between_bursts )[0]
            # print burst_segments
            # chunk through and slice out each burst to analyze seperately
            cur = 0
            the_bursts = []
            for nex in burst_segments:
                # print(cur,nex)
                #print(sliced_data[cur],sliced_data[nex])
                # my_burst.show_spec() .001
                the_bursts.append(np.absolute(this_sample[sliced_data[cur]:sliced_data[nex]]))
                # shift over
                cur = nex+1
            
            # detect the gap between the preamble and data, using same method as above, but on single burst
            # based on specgram, looks like near .001 seconds, anything smaller should be treated as gap between data symbols
            gap_after_preamble = .001*self.working_samp_rate

            gathered_log = gathered_log + ('\n \t the amount of bursts found \n') + str(len(the_bursts))

            # now generate symbols for all bursts in this sample
            for burst in the_bursts:
                # the complex data is converted into magnitudes
                mags = np.absolute(burst)

                # numpy creates an array of positions where the magnitudes are higher than the threshold value
                # this accomplishes nearly the same thing as gnuradio binary slicer, a kind of metadata though
                sliced_data = np.array(np.where(mags > mythresh )[0])

                # the differences between each consecutive number is calculated and placed in this new array
                new = np.array(np.ediff1d(sliced_data))


    ### Work on the preamble
                # if the differences between the sliced data postions are greater than the average distance between the normal bursts,
                # then the number indicates a new burst, and this array holds the starting location of each noew burst.
                preamble_loc = np.where(new > gap_after_preamble )[0]

                # this slice is the preamble: (sliced_data[0],sliced_data[preamble_loc[0]])
                # and this is the base  (sliced_data[preamble_loc[0]+1],the_bursts[0].size)
                the_burst_pramble = np.absolute(burst[sliced_data[0]:sliced_data[preamble_loc[0]]])
                the_burst_base = np.absolute(burst[sliced_data[preamble_loc[0]+1]:burst.size])

                ## now to find widths of minimum symbols --- need to have an idea of minimum sumbol length in samples
                # zooming in on plot of preamble might help -- choose something like half the preamble bits
                # they look about 75 samples wide so...for samp_rate of 4e5, that is 187 microseconds (.0001875 seconds)
                # half that is 
                min_symbol_length = int(.00008125 * self.working_samp_rate)

                preamble_threshold  = np.amax(the_burst_pramble)/2

                #these are the locations where the magnitudes reach the threshold
                preamble_bit_locs = np.array(np.where(the_burst_pramble > preamble_threshold )[0])     

                # opposite list for gaps
                preamble_gap_locs = np.array(np.where(the_burst_pramble < preamble_threshold )[0])  

                # now where the difference between locations is only one, that is part of a consecutive bit, where it is more than one, that is a gap
                # this returns an array of th edifferences in consecutive values
                preamb_thresh_bit_diffs = np.diff(preamble_bit_locs)
                # same for gaps
                preamb_thresh_gap_diffs = np.diff(preamble_gap_locs)

                # this will return an array of where those values are more than our min_symbol length
                preamb_bit_locations = np.where(preamb_thresh_bit_diffs > min_symbol_length)

                #now get the gaps
                preamb_gap_locations = np.where(preamb_thresh_gap_diffs > min_symbol_length)
                # remember that the first location is the beginning of a bit

                ## correcting info lost from the diffs, adding the next bit/gap locations
                #  appending a value that is one more than the previous last value:
                preamb_bit_locations = np.append(preamb_bit_locations,(preamb_bit_locations[0][len(preamb_bit_locations[0])-1])+1)
                preamb_gap_locations = np.append(preamb_gap_locations,(preamb_gap_locations[0][len(preamb_gap_locations[0])-1])+1)

                #and this shows us the "lengths" between bits
                preamb_bit_lengths = np.diff(preamb_bit_locations)
                preamb_gap_lengths = np.diff(preamb_gap_locations)   

                # now combine the lists of locations of gaps and bits, and put them in order
                symbol_locs = np.array([])
                bit_list = []
                gap_list = []

                for locs in preamb_bit_locations:
                    val = preamble_bit_locs[locs]
                    symbol_locs = np.append(symbol_locs,val)
                    bit_list.append(val)
                    
                for locs in preamb_gap_locations:
                    val = preamble_gap_locs[locs]
                    symbol_locs = np.append(symbol_locs,val)
                    gap_list.append(val)

                symbol_list = np.sort(symbol_locs)
                print(symbol_list)
                # presumt the symbols start with a 1?
                symbol_vals = []
                # iterate through the_burst_base, testing the lengths/values of the symbols and assigning symbols
                for pos in symbol_list:
                    test = np.in1d(bit_list,pos)    #test = np.isin(bit_list,pos)
                    if len(np.where(test == True)[0]):
                        symbol_vals.append(1)
                    test = np.in1d(gap_list,pos)    #test = np.isin(gap_list,pos)
                    if len(np.where(test == True)[0]):
                        symbol_vals.append(0)           
    #### Calculating the burst base
                #now to evaluate the remainder of a burst
                # copying the methodology from above for the preamble

                min_symbol_length = int(.00008125 * self.working_samp_rate)
        
                # these are the magnitudes of the preamble part of the burst
                the_burst_base = np.absolute(the_bursts[0][sliced_data[preamble_loc[0]+1]:the_bursts[0].size])

                base_threshold  = np.amax(the_burst_base)/2

                #these are the locations where the magnitudes reach the threshold
                base_bit_locs = np.array(np.where(the_burst_base > base_threshold )[0])
                # oposite list for gaps
                base_gap_locs = np.array(np.where(the_burst_base < base_threshold )[0])

                # now where the difference between locations is only one, that is part of a consecutive bit, where it is more than one, that is a gap
                # this returns an array of th edifferences in consecutive values
                base_thresh_bit_diffs = np.diff(base_bit_locs)
                print("base_thresh_bit_diffs",base_thresh_bit_diffs)

                # same for gaps
                base_thresh_gap_diffs = np.diff(base_gap_locs)
                #   base_bit_locs[base_thresh_gap_diffs[base_bit_locations]] == location in the burst base
                # this will return an array of where those values are more than our min_symbol length
                base_bit_locations = np.where(base_thresh_bit_diffs > min_symbol_length)
                #now get the gaps
                base_gap_locations = np.where(base_thresh_gap_diffs > min_symbol_length)
                # remember that the first location is the beginning of a bit

                ## correcting info lost from the diffs, adding the next bit/gap locations
                #  appending a value that is one more than the previous last value:
                base_bit_locations = np.append(base_bit_locations,(base_bit_locations[0][len(base_bit_locations[0])-1])+1)
                base_gap_locations = np.append(base_gap_locations,(base_gap_locations[0][len(base_gap_locations[0])-1])+1)

                #and this shows us the "lengths" between bits
                base_bit_lengths = np.diff(base_bit_locations)
                base_gap_lengths = np.diff(base_gap_locations)       

                # all the bitwidths look very normal -- 
                # can now populate threshold values for bits - to determine when assigning 11 vs 1
                ones_threshold = np.average(base_bit_lengths)
                zeros_threshold = np.average(base_gap_lengths)                                                 

                # now combine the lists of locations of gaps and bits, and put them in order
                symbol_locs = np.array([])
                bit_list = []
                gap_list = []

                for locs in base_bit_locations:
                    val = base_bit_locs[locs]
                    symbol_locs = np.append(symbol_locs,val)
                    bit_list.append(val)
                    
                for locs in base_gap_locations:
                    val = base_gap_locs[locs]
                    symbol_locs = np.append(symbol_locs,val)
                    gap_list.append(val)

                symbol_list = np.sort(symbol_locs)
                ##print(symbol_list)
                # putting gap in between preamble and rest of data
                symbol_vals = symbol_vals + [0,0,0,0,0,0,0,0,0,0]
                prev = 0
                # iterate through the_burst_base, testing the lengths/values of the symbols and assigning symbols
                for pos in symbol_list:
                    test = np.in1d(bit_list,pos)    # test = np.isin(bit_list,pos)
                    if len(np.where(test == True)[0]):
                        if pos-prev > ones_threshold:
                            symbol_vals.append(1)
                            symbol_vals.append(1)
                        else:
                            symbol_vals.append(1)
                        prev = pos
                        continue
                    test = np.in1d(gap_list,pos)    #test = np.isin(gap_list,pos)
                    if len(np.where(test == True)[0]):
                        if pos-prev > zeros_threshold:
                            symbol_vals.append(0)
                            symbol_vals.append(0)
                        else:
                            symbol_vals.append(0)
                        prev = pos
                # for good measure....probably lost at least one 1, if not two. need a way to check it
                symbol_vals = symbol_vals + [1]
                gathered_log = gathered_log + (str(symbol_vals) + '\n')
                self.l.log(gathered_log)
                gathered_log = ''

            gathered_log = gathered_log +('\n \t -----------end check------------\n')
            self.l.log(gathered_log)

            # clear out this variable for next time
            self.sample_holder = np.array([],dtype='float32')

        return 0#len(output_items[0])