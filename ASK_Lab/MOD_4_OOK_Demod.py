#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: MOD_4_OOK_Demod_qt
# Author: Jon Munson
# Description: This demodulated OOK RAQ file to a binary sliced file
# Generated: Fri Oct 26 06:05:39 2018
##################################################

from gnuradio import blocks
from gnuradio import digital
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser


class MOD_4_OOK_Demod(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "MOD_4_OOK_Demod_qt")

        ##################################################
        # Variables
        ##################################################
        self.working_samp_rate = working_samp_rate = 400000
        self.samp_rate = samp_rate = 8e6
        self.threshold = threshold = .025
        self.offset = offset = 950000
        self.filter_transition = filter_transition = 10e3
        self.filter_decimation = filter_decimation = int(samp_rate/working_samp_rate)
        self.filter_cutoff = filter_cutoff = 100e3

        ##################################################
        # Blocks
        ##################################################
        self.throttle = blocks.throttle(gr.sizeof_gr_complex*1, working_samp_rate,True)
        self.multiply_constant = blocks.multiply_const_vcc((4, ))
        self.freq_xlating_fir_filter = filter.freq_xlating_fir_filter_ccc(filter_decimation, (firdes.low_pass(1, samp_rate, filter_cutoff, filter_transition)), offset, samp_rate)
        self.file_source = blocks.file_source(gr.sizeof_gr_complex*1, 'D:\\OneDrives\\OneDrive - Naval Postgraduate School\\SDRLabs\\solidremote_433_8000000_1', False)
        self.file_sink_0_0 = blocks.file_sink(gr.sizeof_gr_complex*1, 'D:\\OneDrives\\OneDrive - Naval Postgraduate School\\SDRLabs\\sdr-labs-robodojo\\ASK_Lab\\solid_OOK_Rx.sliced.float', False)
        self.file_sink_0_0.set_unbuffered(False)
        self.file_sink_0 = blocks.file_sink(gr.sizeof_float*1, 'D:\\OneDrives\\OneDrive - Naval Postgraduate School\\SDRLabs\\sdr-labs-robodojo\\ASK_Lab\\solid_OOK_Rx.sliced.float', False)
        self.file_sink_0.set_unbuffered(False)
        self.digital_binary_slicer = digital.binary_slicer_fb()
        self.complex_to_mag_squared = blocks.complex_to_mag_squared(1)
        self.blocks_uchar_to_float_0 = blocks.uchar_to_float()
        self.blocks_null_sink_0 = blocks.null_sink(gr.sizeof_float*1)
        self.add_const = blocks.add_const_vff((-1*threshold, ))

        ##################################################
        # Connections
        ##################################################
        self.connect((self.add_const, 0), (self.digital_binary_slicer, 0))
        self.connect((self.blocks_uchar_to_float_0, 0), (self.blocks_null_sink_0, 0))
        self.connect((self.blocks_uchar_to_float_0, 0), (self.file_sink_0, 0))
        self.connect((self.complex_to_mag_squared, 0), (self.add_const, 0))
        self.connect((self.digital_binary_slicer, 0), (self.blocks_uchar_to_float_0, 0))
        self.connect((self.file_source, 0), (self.freq_xlating_fir_filter, 0))
        self.connect((self.freq_xlating_fir_filter, 0), (self.file_sink_0_0, 0))
        self.connect((self.freq_xlating_fir_filter, 0), (self.throttle, 0))
        self.connect((self.multiply_constant, 0), (self.complex_to_mag_squared, 0))
        self.connect((self.throttle, 0), (self.multiply_constant, 0))

    def get_working_samp_rate(self):
        return self.working_samp_rate

    def set_working_samp_rate(self, working_samp_rate):
        self.working_samp_rate = working_samp_rate
        self.set_filter_decimation(int(self.samp_rate/self.working_samp_rate))
        self.throttle.set_sample_rate(self.working_samp_rate)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.set_filter_decimation(int(self.samp_rate/self.working_samp_rate))
        self.freq_xlating_fir_filter.set_taps((firdes.low_pass(1, self.samp_rate, self.filter_cutoff, self.filter_transition)))

    def get_threshold(self):
        return self.threshold

    def set_threshold(self, threshold):
        self.threshold = threshold
        self.add_const.set_k((-1*self.threshold, ))

    def get_offset(self):
        return self.offset

    def set_offset(self, offset):
        self.offset = offset
        self.freq_xlating_fir_filter.set_center_freq(self.offset)

    def get_filter_transition(self):
        return self.filter_transition

    def set_filter_transition(self, filter_transition):
        self.filter_transition = filter_transition
        self.freq_xlating_fir_filter.set_taps((firdes.low_pass(1, self.samp_rate, self.filter_cutoff, self.filter_transition)))

    def get_filter_decimation(self):
        return self.filter_decimation

    def set_filter_decimation(self, filter_decimation):
        self.filter_decimation = filter_decimation

    def get_filter_cutoff(self):
        return self.filter_cutoff

    def set_filter_cutoff(self, filter_cutoff):
        self.filter_cutoff = filter_cutoff
        self.freq_xlating_fir_filter.set_taps((firdes.low_pass(1, self.samp_rate, self.filter_cutoff, self.filter_transition)))


def main(top_block_cls=MOD_4_OOK_Demod, options=None):

    tb = top_block_cls()
    tb.start()
    tb.wait()


if __name__ == '__main__':
    main()
