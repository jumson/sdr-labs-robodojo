#!/opt/conda/bin python
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: MOD_4_OOK_Demod
# Author: Jon Munson
# Description: This demodulated OOK RAW file to a binary sliced file
# Generated: Mon Oct 22 16:43:53 2018
##################################################

from gnuradio import blocks
from gnuradio import digital
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser

## Setup the option parser
parser = OptionParser()
parser.add_option("-i", "--infile", dest="infile",
                  help="what raw iq data file to use?", metavar="INFILE")
parser.add_option("-o", "--outfile", dest="outfile",
                  help="what data file to output?", metavar="OUTFILE")       
parser.add_option("-s", "--samp_rate", dest="samp_rate", type="float", default=8e6,
                  help="what is the input file sample rate?", metavar="SAMP_RATE")    
parser.add_option("-t", "--threshold", dest="threshold", type="float", default=0.5,
                  help="what is the threshold for slicing?", metavar="THRESHOLD")                  
parser.add_option("-f", "--offset", dest="offset", type="float", default=0.0,
                  help="what is the offset from center?", metavar="OFFSET")      
parser.add_option("-r", "--transition", dest="transition", type="float", default=10e3,
                  help="what is the transition width for lowpass filter?", metavar="TRANSITION")      
parser.add_option("-w", "--working_samp", dest="working_samp", type="float", default=400e3,
                  help="what is the working sample rate?", metavar="WORKING_SAMPLE_RATE")      
parser.add_option("-c", "--cutoff", dest="cutoff", type="float", default=100e3,
                  help="what is the filter cuttoff, stop?", metavar="CUTOFF")      
parser.add_option("-q", "--quiet",
                  action="store_false", dest="verbose", default=True,
                  help="don't print status messages to stdout")

(options, args) = parser.parse_args()
# this wil be a script that takes several arguments and slices a raw ook modulated sample


# We are inside some application


if not options.infile or not options.outfile:
    print("\t You must specify an infile and an outfile. \n\t Sample rate defaults to 8.0Ms \n\t threshold defaults to 0.5")
    exit()

#print(options)

### OOK_Demod  script
ook_slicer = gr.top_block() # Define the container

        ##################################################
        # Variables
        ##################################################
ook_slicer.working_samp_rate = working_samp_rate = options.working_samp
ook_slicer.samp_rate = samp_rate = options.samp_rate
ook_slicer.threshold = threshold = options.threshold
ook_slicer.offset = offset = options.offset
ook_slicer.filter_transition = filter_transition = options.transition
ook_slicer.filter_decimation = filter_decimation = int(samp_rate/working_samp_rate)
ook_slicer.filter_cutoff = filter_cutoff = options.cutoff
ook_slicer.filename_sliced = filename_sliced = options.outfile
ook_slicer.filename_raw_rx = filename_raw_rx = options.infile

        ##################################################
        # Blocks
        ##################################################

ook_slicer.throttle = blocks.throttle(gr.sizeof_gr_complex*1, working_samp_rate,True)
ook_slicer.multiply_constant = blocks.multiply_const_vcc((4, ))
ook_slicer.freq_xlating_fir_filter = filter.freq_xlating_fir_filter_ccc(filter_decimation, (firdes.low_pass(1, samp_rate, filter_cutoff, filter_transition)), offset, samp_rate)
ook_slicer.file_source = blocks.file_source(gr.sizeof_gr_complex*1, filename_raw_rx, False)
ook_slicer.file_sink_0 = blocks.file_sink(gr.sizeof_float*1, filename_sliced, False)
ook_slicer.file_sink_0.set_unbuffered(False)
ook_slicer.digital_binary_slicer = digital.binary_slicer_fb()
ook_slicer.complex_to_mag_squared = blocks.complex_to_mag_squared(1)
ook_slicer.blocks_uchar_to_float_0 = blocks.uchar_to_float()
ook_slicer.add_const = blocks.add_const_vff((-1*threshold, ))

        ##################################################
        # Connections
        ##################################################

ook_slicer.connect((ook_slicer.add_const, 0), (ook_slicer.digital_binary_slicer, 0))
ook_slicer.connect((ook_slicer.blocks_uchar_to_float_0, 0), (ook_slicer.file_sink_0, 0))
ook_slicer.connect((ook_slicer.complex_to_mag_squared, 0), (ook_slicer.add_const, 0))
ook_slicer.connect((ook_slicer.digital_binary_slicer, 0), (ook_slicer.blocks_uchar_to_float_0, 0))
ook_slicer.connect((ook_slicer.file_source, 0), (ook_slicer.freq_xlating_fir_filter, 0))
ook_slicer.connect((ook_slicer.freq_xlating_fir_filter, 0), (ook_slicer.throttle, 0))
ook_slicer.connect((ook_slicer.multiply_constant, 0), (ook_slicer.complex_to_mag_squared, 0))
ook_slicer.connect((ook_slicer.throttle, 0), (ook_slicer.multiply_constant, 0))

# This thing now just pumps it out!

ook_slicer.start() # Start the flow graph
ook_slicer.wait() # wait for it to finish and return
# Do some more incredible and fascinating stuff here?
# perhaps stdout a preview?