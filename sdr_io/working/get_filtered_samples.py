#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Get Filtered Samples
# Author: Jon Munson
# Description: Run to get filtered sampels ising osmocom source
# Generated: Sun Oct 28 17:51:19 2018
##################################################

from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import osmosdr
import time

## Setup the option parser
parser = OptionParser()
parser.add_option("-o", "--outfile", dest="outfile",
                  help="what data file to output?", metavar="OUTFILE")       
parser.add_option("-s", "--samp_rate", dest="samp_rate", type="float", default=2e6,
                  help="what is the input file sample rate?", metavar="SAMP_RATE")                    
parser.add_option("-f", "--frequency", dest="target_freq", type="float", default=315.018e6,
                  help="what is the target freq?", metavar="TARGET_FREQ")    
parser.add_option("-d", "--duration", dest="duration", type="int", default=10,
                  help="How many seconds to capture?", metavar="DURATION")  
parser.add_option("-q", "--quiet",
                  action="store_false", dest="verbose", default=True,
                  help="don't print status messages to stdout")

(options, args) = parser.parse_args()
# this wil be a script that takes several arguments and outputs a raw filtered sample

if not options.outfile:
    print("\t You must specify an outfile.  \n\t Sample rate defaults to 2.0Ms \n\t Freq defaults to 315MHz \n\t Duration defaults to 10 seconds of capture")
    exit()

class get_filtered_samples(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "Get Filtered Samples")

        ##################################################
        # Variables
        ##################################################
        self.working_samp_rate = working_samp_rate = 400000
        self.target_freq = target_freq = options.target_freq
        self.samp_rate = samp_rate = options.samp_rate
        self.offset = offset = -200e3
        self.freq = freq = target_freq+offset
        self.filter_transition = filter_transition = 10e3
        self.filter_decimation = filter_decimation = int(samp_rate/working_samp_rate)
        self.filter_cutoff = filter_cutoff = .10e6
        self.filename = filename = options.outfile
        self.bandwidth = bandwidth = 400e3
        self.duration = options.duration

        ##################################################
        # Blocks
        ##################################################
        self.freq_xlating_fir_filter = filter.freq_xlating_fir_filter_ccc(filter_decimation, (firdes.low_pass(1, samp_rate, filter_cutoff, filter_transition)), offset, samp_rate)
        self.file_sink = blocks.file_sink(gr.sizeof_gr_complex*1, filename, False)
        self.file_sink.set_unbuffered(False)
        self.HackRF_source_0 = osmosdr.source( args="numchan=" + str(1) + " " + "soapy=0,driver=hackrf" )
        self.HackRF_source_0.set_sample_rate(samp_rate)
        self.HackRF_source_0.set_center_freq(freq, 0)
        self.HackRF_source_0.set_freq_corr(0, 0)
        self.HackRF_source_0.set_dc_offset_mode(0, 0)
        self.HackRF_source_0.set_iq_balance_mode(0, 0)
        self.HackRF_source_0.set_gain_mode(False, 0)
        self.HackRF_source_0.set_gain(0, 0)
        self.HackRF_source_0.set_if_gain(20, 0)
        self.HackRF_source_0.set_bb_gain(20, 0)
        self.HackRF_source_0.set_antenna('', 0)
        self.HackRF_source_0.set_bandwidth(bandwidth, 0)


        ##################################################
        # Connections
        ##################################################
        self.connect((self.HackRF_source_0, 0), (self.freq_xlating_fir_filter, 0))
        self.connect((self.freq_xlating_fir_filter, 0), (self.file_sink, 0))

    def get_working_samp_rate(self):
        return self.working_samp_rate

    def set_working_samp_rate(self, working_samp_rate):
        self.working_samp_rate = working_samp_rate
        self.set_filter_decimation(int(self.samp_rate/self.working_samp_rate))

    def get_target_freq(self):
        return self.target_freq

    def set_target_freq(self, target_freq):
        self.target_freq = target_freq
        self.set_freq(self.target_freq-self.offset)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.set_filter_decimation(int(self.samp_rate/self.working_samp_rate))
        self.freq_xlating_fir_filter.set_taps((firdes.low_pass(1, self.samp_rate, self.filter_cutoff, self.filter_transition)))
        self.HackRF_source_0.set_sample_rate(self.samp_rate)

    def get_offset(self):
        return self.offset

    def set_offset(self, offset):
        self.offset = offset
        self.set_freq(self.target_freq-self.offset)
        self.freq_xlating_fir_filter.set_center_freq(self.offset)

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq
        self.HackRF_source_0.set_center_freq(self.freq, 0)

    def get_filter_transition(self):
        return self.filter_transition

    def set_filter_transition(self, filter_transition):
        self.filter_transition = filter_transition
        self.freq_xlating_fir_filter.set_taps((firdes.low_pass(1, self.samp_rate, self.filter_cutoff, self.filter_transition)))

    def get_filter_decimation(self):
        return self.filter_decimation

    def set_filter_decimation(self, filter_decimation):
        self.filter_decimation = filter_decimation

    def get_filter_cutoff(self):
        return self.filter_cutoff

    def set_filter_cutoff(self, filter_cutoff):
        self.filter_cutoff = filter_cutoff
        self.freq_xlating_fir_filter.set_taps((firdes.low_pass(1, self.samp_rate, self.filter_cutoff, self.filter_transition)))

    def get_filename(self):
        return self.filename

    def set_filename(self, filename):
        self.filename = filename
        self.file_sink.open(self.filename)

    def get_bandwidth(self):
        return self.bandwidth

    def set_bandwidth(self, bandwidth):
        self.bandwidth = bandwidth
        self.HackRF_source_0.set_bandwidth(self.bandwidth, 0)


def main(top_block_cls=get_filtered_samples, options=None):

    tb = top_block_cls()
    start = time.time()
    print('start time on radio:',time.localtime(start))
    tb.start()
    duration = tb.duration
    try:
        while time.time() - start < duration:
            print(time.time() - start)
            time.sleep(1)
        tb.stop()
        tb.wait()
        exit()
    except EOFError:
        exit()
        pass
    tb.stop()
    tb.wait()
    exit()


if __name__ == '__main__':
    main()
