#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: MOD_1_OOK_Message_Generator
# Author: Jon Munson
# Description: This simply generates a messge and saves it in a raw IQ file
# Generated: Tue Oct 30 05:29:07 2018
##################################################

from gnuradio import analog
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import osmosdr
import time


## Setup the option parser
parser = OptionParser()
parser.add_option("-o", "--outfile", dest="outfile",
                  help="what data file to output?", metavar="OUTFILE")       
parser.add_option("-s", "--samp_rate", dest="samp_rate", type="float", default=2e6,
                  help="what is the input file sample rate?", metavar="SAMP_RATE")                    
parser.add_option("-f", "--frequency", dest="target_freq", type="float", default=315.018e6,
                  help="what is the target freq?", metavar="TARGET_FREQ")    
parser.add_option("-d", "--duration", dest="duration", type="int", default=10,
                  help="How many seconds to capture?", metavar="DURATION")  
parser.add_option("-v", "--vector", dest="vector", type="str", default='1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1',
                  help="Supply the vector to transmit as comma seperated values '1,0,1,0' ", metavar="VECTOR")  
parser.add_option("-r", "--repeats", dest="repeats", type="int", default=5,
                  help=" how many times to repeat the burst? ", metavar="REPEATS") 
parser.add_option("-g", "--gap", dest="gap", type="float", default=.03,
                  help="how long is the gap between bursts? (seconds) ", metavar="GAP") 
parser.add_option("-p", "--pulse_width", dest="pulse_width", type="float", default=.0002,
                  help="how long is the short pulse? (seconds) ", metavar="PULSE_WIDTH") 
parser.add_option("-i", "--if_gain", dest="if_gain", type="int", default=20,
                  help="if gain, form 0 to 47 ", metavar="IF_GAIN")
parser.add_option("-q", "--quiet",
                  action="store_false", dest="verbose", default=True,
                  help="don't print status messages to stdout")

(options, args) = parser.parse_args()
# this wil be a script that takes several arguments and outputs a raw filtered sample

if not options.vector:
    print("\t You must specify data to send -> '0,1,0,1,0,1,0,0,0,1,1,1,0,0,0,'.  \n\t Sample rate defaults to 2.0Ms \n\t Freq defaults to 315MHz")
    exit()
if options.vector:
    data = options.vector.replace("'","")
    splitt = data.split(',')
    #print("\n\n\ttranslating vector as a string to python tuple",options.vector,"\n",splitt)
    my_int_list = []
    for i in range(len(splitt)):
        my_int_list.append(int(splitt[i]))
    tx_data = tuple(my_int_list)
    #print("\ttx_data length:",len(tx_data),"\n\n tx_data:\n",tx_data)
    ## create the gap and put it at the end
    #gap seconds * samp_rate (samps/second) == samples
    #my_gap = tuple([0])*int((options.gap*options.samp_rate))
    #tx_data = tx_data + my_gap
    # now repetitions
    #tx_data = tx_data * options.repeats
    print("\ttotal tx_data length:",len(tx_data),"\n\n")


class MOD_1_OOK_Message_Generator(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "MOD_1_OOK_Message_Generator")

        ##################################################
        # Variables
        ##################################################
        self.target_freq = target_freq = options.target_freq
        self.samp_rate = samp_rate = options.samp_rate
        self.pulse_len = pulse_len = options.pulse_width
        self.offset = offset = -200e3
        self.samples = samples = int(pulse_len*samp_rate)
        self.freq = freq = target_freq-offset
        self.filename = filename = "./input.raw"
        self.repeats = options.repeats
        self.gap = options.gap
        self.tx_data = tx_data

        ##################################################
        # Blocks
        ##################################################
        self.vector_source = blocks.vector_source_f(tx_data, False, 1, [])
        self.repeat = blocks.repeat(gr.sizeof_float*1, samples)
        self.osmosdr_sink_0 = osmosdr.sink( args="numchan=" + str(1) + " " + "soapy=0,driver=hackrf" )
        self.osmosdr_sink_0.set_sample_rate(samp_rate)
        self.osmosdr_sink_0.set_center_freq(freq, 0)
        self.osmosdr_sink_0.set_freq_corr(0, 0)
        self.osmosdr_sink_0.set_gain(14, 0)
        self.osmosdr_sink_0.set_if_gain(options.if_gain, 0)
        self.osmosdr_sink_0.set_bb_gain(20, 0)
        self.osmosdr_sink_0.set_antenna('', 0)
        self.osmosdr_sink_0.set_bandwidth(0, 0)

        self.multiply = blocks.multiply_vcc(1)
        self.float_to_complex = blocks.float_to_complex(1)
        self.analog_sig_source = analog.sig_source_c(samp_rate, analog.GR_COS_WAVE, offset, 1, 0)
        self.analog_const_source = analog.sig_source_f(0, analog.GR_CONST_WAVE, 0, 0, 0)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_const_source, 0), (self.float_to_complex, 1))
        self.connect((self.analog_sig_source, 0), (self.multiply, 1))
        self.connect((self.float_to_complex, 0), (self.multiply, 0))
        self.connect((self.multiply, 0), (self.osmosdr_sink_0, 0))
        self.connect((self.repeat, 0), (self.float_to_complex, 0))
        self.connect((self.vector_source, 0), (self.repeat, 0))

    def get_target_freq(self):
        return self.target_freq

    def set_target_freq(self, target_freq):
        self.target_freq = target_freq
        self.set_freq(self.target_freq+self.offset)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.set_samples(int(self.pulse_len*self.samp_rate))
        self.osmosdr_sink_0.set_sample_rate(self.samp_rate)
        self.analog_sig_source.set_sampling_freq(self.samp_rate)

    def get_pulse_len(self):
        return self.pulse_len

    def set_pulse_len(self, pulse_len):
        self.pulse_len = pulse_len
        self.set_samples(int(self.pulse_len*self.samp_rate))

    def get_offset(self):
        return self.offset

    def set_offset(self, offset):
        self.offset = offset
        self.set_freq(self.target_freq+self.offset)
        self.analog_sig_source.set_frequency(self.offset)

    def get_samples(self):
        return self.samples

    def set_samples(self, samples):
        self.samples = samples
        self.repeat.set_interpolation(self.samples)

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq
        self.osmosdr_sink_0.set_center_freq(self.freq, 0)

    def get_filename(self):
        return self.filename

    def set_filename(self, filename):
        self.filename = filename


def main(top_block_cls=MOD_1_OOK_Message_Generator, options=None):

    tb = top_block_cls()
    
    the_gap = tb.gap
    #print('start time on radio:',time.localtime(start))
    try:
        for each in range(tb.repeats):
            print("Tx center at ",tb.get_freq()," to offset ",tb.get_offset()," with pusle length ",tb.get_pulse_len(), " and sample rate of ",tb.get_samp_rate())
            print("Tx with gap ",tb.gap," and repeating ",tb.repeats)
            print("data to tx: ",tb.tx_data)
            #tb.run()
            tb.start()
            tb.wait()
            #tb.stop()
            time.sleep(the_gap)
        exit()
    except EOFError:
        tb.stop()
        tb.wait()
        exit()
        pass
    tb.stop()
    tb.wait()
    exit()


if __name__ == '__main__':
    main()
