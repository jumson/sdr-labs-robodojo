#!/usr/bin/env python3
# tablet with stickers, --- left station
#https://docs.python.org/3.6/library/sched.html
import sched, time
import subprocess
import json, decimal
from optparse import OptionParser

## Setup the option parser
parser = OptionParser()
parser.add_option("-n", "--node", dest="node", type="int", default=1,
                  help="which node? (1=tx then rx, 2=rx then tx) -- for testing sets", metavar="NODE") 
parser.add_option("-t", "--time", dest="time", type="int", default=1,
                  help="how many minutes to start set? From the op of the next minute", metavar="TIME") 
(options, args) = parser.parse_args()
##schedule a bunch of actions based on absolute times

# the action should be a function that runs a command line, and then checks for exit or early exit, and responds

def sdr_listen(freq,outfile,samp_rate,duration,sched):
    # this command will need to be run: 'sudo ./get_filtered_samples.py -o outfile -s 2e6 -f 315e6 -d duration'
    commands = 'sudo ./get_filtered_samples.py -o ' + outfile + ' -s ' + str(samp_rate) + ' -f ' + str(freq) + ' -d ' + str(duration)
    args = commands.split()
    start = time.time()
    print('listen_process starting: ',start,args[3])
    proc = subprocess.Popen(args)
    proc.wait(timeout=duration+2)
    end_time =  time.time() - start
    # if it returns a non-zero code, reschedule it immediately
    if proc.returncode is not 0:
        sched.enter(0.1, 1, sdr_listen, argument=(freq,outfile,samp_rate,duration,sched))
        print('###RESCHEDULING LISTEN',outfile,proc.returncode)
        return
    # if successfully records, schedule the decoder relative time, lower priority
    if proc.returncode is 0:
        print('****SCHEDULING DECODE',outfile,proc.returncode)
        sched.enter(0.1, 5, sdr_decode, argument=(freq,outfile,samp_rate,sched))
        print('process returned well!',proc.returncode)

def sdr_decode(freq,infile,samp_rate,sched):
    # this command will need to be run: 'sudo ./get_filtered_samples.py -o outfile -s 2e6 -f 315e6 -d duration'
    commands = 'sudo ./raw_to_symbols.py -i ' + infile + ' -s ' + str(samp_rate) + ' -f ' + str(freq)
    args = commands.split()
    start = time.time()
    print('process starting: ',start,args[3])
    proc = subprocess.Popen(args)
    proc.wait(timeout=duration+2)
    end_time =  time.time() - start
    # if it returns a non-zero code, reschedule it immediately
    if proc.returncode is not 0:
        sched.enter(0.1, 5, sdr_decode, argument=(freq,infile,samp_rate,sched))
        print('###RESCHEDULING DECODE',infile,proc.returncode)
        return
    if proc.returncode is 0:
        print('****SCHEDULING SHOW BURST',infile+'.json',proc.returncode)
        sched.enter(0.1, 5, show_burst, argument=(infile+'.json',s))
        print('sample decoded returned!',proc.returncode,infile)

def show_burst(infile,sched):
    with open(infile) as burst_d:
        data = json.load(burst_d,parse_float=decimal.Decimal)
        burst_data = json.loads(data)
        for burst in range(burst_data['sample_bursts_detected']):
            print("burst ",burst+1," base64: ",burst_data['bursts'][str(burst+1)]['base64'])
            print("burst ",burst+1,"    hex: ",burst_data['bursts'][str(burst+1)]['hexlify'])

def sdr_tx(freq,data,samp_rate,repeats,gap,pulse_width,sched):
    # this command will need to be run: 'sudo ./tx.py -v '0,,1,0,1,0' -f 315018e3 -r 5 -g .05'
    commands = 'sudo ./tx.py -v ' + "'" + data + "'" + ' -s ' + str(samp_rate) + ' -f ' + str(freq) + ' -r ' + str(repeats) + ' -g ' + str(gap) + ' -i 47' + ' -p ' + str(pulse_width)
    args = commands.split()
    start = time.time()
    print('sdr_tx starting: ',start,args[3])
    proc = subprocess.Popen(args)
    proc.wait(timeout=duration+2)
    end_time =  time.time() - start
    # if it returns a non-zero code, reschedule it immediately
    if proc.returncode is not 0:
        sched.enter(0.1, 1, sdr_tx, argument=(freq,data,samp_rate,repeats,gap,pulse_width,sched))
        print('###RESCHEDULING SDR_TX:',data,proc.returncode)
        return
    # if successfully records, schedule the decoder relative time, lower priority
    #if proc.returncode is 0:
    #    print('****SCHEDULING DECODE',outfile,proc.returncode)
    #    sched.enter(0.1, 5, sdr_decode, argument=(freq,outfile,samp_rate,sched))
    #    print('process returned well!',proc.returncode)


## Timer / scheduler stuff goes here
# scheduler.enterabs(time, priority, action, argument=(), kwargs={})
s = sched.scheduler(time.time, time.sleep)

# set a base time -- such as 00:00 at 29 October 2018
# time_29_10_2018 = 1540796400.5318391
# and iterate off of that to produce absolute values
# GMT?
time_29_10_2018 = time_base =  1540796400.5318391
hours_24 = 86400
hours_12 = hours_24/2
hours_6 = hours_12/2
hours_3 = hours_6/2
hour = hours_3/3    #3600
minute = hour/60    #60


#time_base = time_29_10_2018 + hours_24 + hours_6 + hours_3 + (hour*2)+  (minute*31)  # 0912, Tuesday, 30 October

#time to start recording
#start_rec = time_base  # 0500, Tuesday, 30 October
#time to start recording
#start_rec = time.time()  #time_base+hours_12
start_base = time.time()
more_secs = 60-time.localtime(start_base).tm_sec   # this will add enough seconds to the start time to start at top of next minute
more_mins = 60*options.time
start_next = start_base+more_mins+more_secs
# how many times? 10 seconds on, 10 seconds off, == three times a minute for an hour

samp_rate_tx = 8e6
samp_rate_rx = 4e6
duration = 5
times = 2
gap = .1
repeats = 6
pulse_width = .006
freq = '315018e3'
data = '1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1'
data = data.replace(' ','')
for x in range(times):
    if options.node == 2:
        the_time_rx = start_next+(5)+(x*(duration*3)) #steps duration times 3
        the_time_tx = start_next+(5+duration+1)+(x*(duration*3))
    if options.node == 1:
        the_time_rx = start_next+(5+duration)+(x*(duration*3))#steps duration times 3
        the_time_tx = start_next+(5+1)+(x*(duration*3)) 
    outfile1 = 'gdo_'+str(the_time_rx)+'.raw'

    s.enterabs(the_time_rx, 1, sdr_listen, argument=(freq,outfile1,samp_rate_rx,duration,s))
    s.enterabs(the_time_tx, 1, sdr_tx, argument=(freq,data,samp_rate_tx,repeats,gap,pulse_width,s))
#outfile2 = 'solid_'+str(start_rec+(10+(times*minute)))+'.raw'
#s.enterabs(start_rec+(15), 2, sdr_listen, argument=('433943e3',outfile2,samp_rate,duration,s))

#outfile3 = 'doorbell_'+str(start_rec+(10+(times*minute)))+'.raw'
#s.enterabs(start_rec+(25), 3, sdr_listen, argument=('314999e3',outfile3,samp_rate,duration,s))

for job in s.queue:
    print(job.action,time.localtime(job.time))

s.run()