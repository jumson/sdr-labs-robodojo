#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Doorbell_spoof
# Author: Jon Munson
# Description: This simply generates a messge and saves it in a raw IQ file
# Generated: Tue Oct 30 20:14:56 2018
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt4 import Qt
from gnuradio import analog
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio import qtgui
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import osmosdr
import sip
import sys
import time
from gnuradio import qtgui


class Doorbell_spoof(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Doorbell_spoof")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Doorbell_spoof")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "Doorbell_spoof")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())

        ##################################################
        # Variables
        ##################################################
        self.target_freq = target_freq = 315e6
        self.samp_rate = samp_rate = 20e6
        self.pulse_len = pulse_len = .000350
        self.offset = offset = -200e3
        self.samples = samples = int(pulse_len*samp_rate)
        self.freq = freq = target_freq+offset
        self.filename = filename = "D:\OneDrives\OneDrive - Naval Postgraduate School\\thesis\pinode_gitlab\examples_tests\ook\Modules\GDO_Generated.RAW"

        ##################################################
        # Blocks
        ##################################################
        self.vector_source = blocks.vector_source_f((1,0,0,1,0,1,1,0,1,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,1,1,0,1,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0), True, 1, [])
        self.repeat = blocks.repeat(gr.sizeof_float*1, samples)
        self.qtgui_sink_x_0_0 = qtgui.sink_c(
        	1024, #fftsize
        	firdes.WIN_BLACKMAN_hARRIS, #wintype
        	0, #fc
        	samp_rate, #bw
        	"", #name
        	True, #plotfreq
        	True, #plotwaterfall
        	True, #plottime
        	True, #plotconst
        )
        self.qtgui_sink_x_0_0.set_update_time(1.0/10)
        self._qtgui_sink_x_0_0_win = sip.wrapinstance(self.qtgui_sink_x_0_0.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_sink_x_0_0_win)

        self.qtgui_sink_x_0_0.enable_rf_freq(False)



        self.osmosdr_source_0 = osmosdr.source( args="numchan=" + str(1) + " " + "soapy=2,driver=hackrf" )
        self.osmosdr_source_0.set_sample_rate(samp_rate)
        self.osmosdr_source_0.set_center_freq(freq, 0)
        self.osmosdr_source_0.set_freq_corr(0, 0)
        self.osmosdr_source_0.set_dc_offset_mode(0, 0)
        self.osmosdr_source_0.set_iq_balance_mode(0, 0)
        self.osmosdr_source_0.set_gain_mode(False, 0)
        self.osmosdr_source_0.set_gain(14, 0)
        self.osmosdr_source_0.set_if_gain(24, 0)
        self.osmosdr_source_0.set_bb_gain(24, 0)
        self.osmosdr_source_0.set_antenna('', 0)
        self.osmosdr_source_0.set_bandwidth(0, 0)

        self.multiply = blocks.multiply_vcc(1)
        self.float_to_complex = blocks.float_to_complex(1)
        self.blocks_null_sink_0 = blocks.null_sink(gr.sizeof_gr_complex*1)
        self.blocks_file_sink_0 = blocks.file_sink(gr.sizeof_gr_complex*1, 'D:\\OneDrives\\OneDrive - Naval Postgraduate School\\SDRLabs\\genie_gdo_recRAW', False)
        self.blocks_file_sink_0.set_unbuffered(False)
        self.analog_sig_source = analog.sig_source_c(samp_rate, analog.GR_COS_WAVE, -offset, 1, 0)
        self.analog_const_source = analog.sig_source_f(0, analog.GR_CONST_WAVE, 0, 0, 0)

        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_const_source, 0), (self.float_to_complex, 1))
        self.connect((self.analog_sig_source, 0), (self.multiply, 1))
        self.connect((self.float_to_complex, 0), (self.multiply, 0))
        self.connect((self.multiply, 0), (self.blocks_null_sink_0, 0))
        self.connect((self.osmosdr_source_0, 0), (self.blocks_file_sink_0, 0))
        self.connect((self.osmosdr_source_0, 0), (self.qtgui_sink_x_0_0, 0))
        self.connect((self.repeat, 0), (self.float_to_complex, 0))
        self.connect((self.vector_source, 0), (self.repeat, 0))

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "Doorbell_spoof")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_target_freq(self):
        return self.target_freq

    def set_target_freq(self, target_freq):
        self.target_freq = target_freq
        self.set_freq(self.target_freq+self.offset)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.set_samples(int(self.pulse_len*self.samp_rate))
        self.qtgui_sink_x_0_0.set_frequency_range(0, self.samp_rate)
        self.osmosdr_source_0.set_sample_rate(self.samp_rate)
        self.analog_sig_source.set_sampling_freq(self.samp_rate)

    def get_pulse_len(self):
        return self.pulse_len

    def set_pulse_len(self, pulse_len):
        self.pulse_len = pulse_len
        self.set_samples(int(self.pulse_len*self.samp_rate))

    def get_offset(self):
        return self.offset

    def set_offset(self, offset):
        self.offset = offset
        self.set_freq(self.target_freq+self.offset)
        self.analog_sig_source.set_frequency(-self.offset)

    def get_samples(self):
        return self.samples

    def set_samples(self, samples):
        self.samples = samples
        self.repeat.set_interpolation(self.samples)

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq
        self.osmosdr_source_0.set_center_freq(self.freq, 0)

    def get_filename(self):
        return self.filename

    def set_filename(self, filename):
        self.filename = filename


def main(top_block_cls=Doorbell_spoof, options=None):

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
