#!/opt/conda/bin python
# 
##################################################
# Some functions I use frequently
# 
# Author: Jon Munson
# Description: 
# 
##################################################
# example from https://www.geeksforgeeks.org/writing-files-background-python/
# Inherting the base class 'Thread'
import threading
class AsyncWrite(threading.Thread): 
 
    def __init__(self, out):
 
        # calling superclass init
        threading.Thread.__init__(self) 
        self.text = ''
        self.out = out
 
    def log(self, text):
        self.text = text
        f = open(self.out, "a+")
        f.write(self.text)
        f.close()
 
        #print("Finished background file write to", self.out)
        # usage:     background = AsyncWrite('stuff to write\n', filename)
                #    background.start()
        # usage:     l = AsyncWrite(filename)
                #    l.log('log this \n')

class Colors:
    # Foreground:
    MAGENTA = '\033[95m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    WHITE = '\033[97m'
    BLACK = '\033[90m'
    # Formatting
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'    
    # End colored text
    END = '\033[0m'
    NC ='\x1b[0m' # No Color
    DEFAULT = '\033[99m'

c = Colors()

class Slices:
    def __init__(self, 
                 symbols=[], 
                 ones_long=0, 
                 ones_short=0, 
                 zeros_long=0, 
                 zeros_short=0,
                ones_threshold=0,
                zeros_threshold=0,
                transitions=[],
                burst=[],
                 burst_ends = [],
                samp_rate=400000,
                min_width=20):
        #this will be an array of 0's and 1's
        self.symbols = symbols
        #these are the lengths of each kind of pulse, in microseconds
        self.ones_long = ones_long
        self.ones_short = ones_short
        self.zeros_long = zeros_long
        self.zeros_short = zeros_short
        # these are the values used t odiscriminate between longs and shorts
        self.ones_threshold = ones_threshold
        self.zeros_threshold = zeros_threshold
        # these are the positions in the slices where there are transitions
        self.transitions = transitions
        # this is the boolean burst itself, starts and ends with Trues
        # usually created with a sliced file: dat_sliced = np.fromfile(infile, dtype="float32")
                                            # my_slices.burst = np.array(dat_sliced,np.bool)
        self.burst = burst
        # The sample rate at which the slices/samples were processed
        self.samp_rate = samp_rate
        # the amount of samples in a row to suppose a legitimate pulse
        self.min_width = min_width
        self.burst_ends = burst_ends

class Burst:
    def __init__(self, 
                 name='',
                 modulation='',
                 encoding='',
                 slices=Slices(),
                 samp_rate=8e6,
                 center_freq=0,
                 working_samp_rate=4e5,
                 offset=0,
                 threshold=.5,
                 filter_cutoff=100e3,
                 filter_transition=10e3,
                 raw_input_file='',
                 demodulated_output='',
                 data_arr=[]):
        self.name = name
        self.modulation = modulation
        self.encoding = encoding
        self.slices = slices
        self.samp_rate = samp_rate
        self.working_samp_rate = working_samp_rate
        self.offset = offset
        self.threshold = threshold
        self.filter_cutoff = filter_cutoff
        self.filter_transition = filter_transition
        self.raw_input_file = raw_input_file
        self.demodulated_output = demodulated_output
        self.center_freq = center_freq
        self.data_arr = data_arr
        
    def load_data(self):
        try:
            self.data_arr = np.fromfile(self.raw_input_file, dtype="complex64")
        except:
            pass
            #print("ensure Burst.raw_input_file has a legit sample path/file")
    
    def duration(self):
        if len(self.data_arr) == 0:
            self.load_data()
        #print(len(self.data_arr)/self.samp_rate,'seconds')

    
def get_transitions(sliced_data):
    #create boolean values from the floats -- zero is false, everything else is True
    numpy_bools = np.array(sliced_data,np.bool)
    
    # first get value of where transitions occure
    diff_pos = np.diff(numpy_bools)
    
    # these values are a list of positions of transitions
    diff_pos_loc = np.where(diff_pos)[0]
    
    return diff_pos_loc

def get_bad_bursts(transitions, bad_burst_width=20):
    # roll it left so I can ieterate over both and compare current to next values
    next_transitions = np.roll(transitions,-1)
    
    # this stores the location of 'illegitimate' bursts, one-offs
    abberent_bursts = []
    
    # now we look for the illegitimate bursts and get a list of them:
    counter = 0
    for current_loc, next_loc in zip(transitions, next_transitions):
        if next_loc < current_loc:
            burst_slice_range = [next_loc, current_loc]
            continue
        if next_loc - current_loc < bad_burst_width:
            abberent_bursts.append(counter)
        counter = counter + 1
    return abberent_bursts

def get_burst_widths(sliced_data,transistion_list):
    true_width_list = []
    false_width_list = []
    counter = 0
    numpy_bools = np.array(sliced_data,np.bool)
    transitions_next = np.roll(transistion_list,-1)
    
    for current_loc, next_loc in zip(transistion_list, transitions_next):
        if next_loc < current_loc:
            burst_slice_range = [next_loc, current_loc]
            continue
        width = next_loc-(current_loc+1)
        value = sum(numpy_bools[current_loc+1:next_loc])
        if value > 10:
            true_width_list.append(width)
        else:
            false_width_list.append(width)
    return [true_width_list,false_width_list,burst_slice_range]

def get_discriminators(my_slices):
    logfile = 'D:\OneDrives\OneDrive - Naval Postgraduate School\SDRLabs\sdr-labs-robodojo\ASK_Lab\flowgraph_data.log'
    l = AsyncWrite(logfile)
    gathered = ''
    my_slices.transitions = get_clean_trans(my_slices.burst, my_slices.min_width)
    
    # [true_width_list,false_width_list,burst_slice_range]
    burst_info = get_burst_widths(my_slices.burst,my_slices.transitions)
    gathered = gathered + ('\n burst widths, info:'+str(burst_info))
    
    my_slices.burst_ends = burst_info[2]
    #my_slices.burst = my_slices.burst[(burst_ends[0]+1):(burst_ends[1]+1)]
    try:
        sorted_true = sorted(burst_info[0])
        gathered = gathered + ('\n sorted true', sorted_true)
        true_len = len(sorted_true)
        sorted_false = sorted(burst_info[1])
        gathered = gathered + ('\n sorted false', sorted_false)
        false_len = len(sorted_false)
        gathered = gathered + ('\n slice range (whole thing)',burst_info[2],my_slices.burst.size)
        
        my_slices.ones_threshold = sorted_true[0]+(sorted_true[true_len-1] - sorted_true[0])/2
        my_slices.zeros_threshold = sorted_false[0]+(sorted_false[false_len-1] - sorted_false[0])/2
        l.log(gathered)
    except:
        l.log( gathered + '\n probably index error in get_discriminators')

    return my_slices

def get_clean_trans(sliced_data, min_width = 20):
    # get initial transistion locations
    i_transitions = get_transitions(sliced_data)
    # print('initial transition locations:'+str(i_transitions))
    # get list of spurious, bad, 'illegitimate' bursts
    bad_bursts = get_bad_bursts(transitions=i_transitions, bad_burst_width=min_width)
    # print('bad bursts:'+str(bad_bursts))
    # this deletes abberations, making the authoritative list of transitions
    # print 'bad bursts',bad_bursts
    transitions = np.delete(i_transitions,bad_bursts)
    # print('final transition locations:'+str(transitions))
    return transitions

def get_symbols(my_slices):
    high = False
    last = 0
    high_length_long = []
    high_length_short = []
    low_length_long = []
    low_length_short = []
    
    for val in my_slices.transitions:
        pulse = 0
        pulse = val - last
        last = val
        if high:
            if pulse > my_slices.ones_threshold:
                my_slices.symbols.append(1)
                my_slices.symbols.append(1)
                high_length_long.append(pulse)
            else:
                my_slices.symbols.append(1)
                high_length_short.append(pulse)
            high = False
            continue
        if not high:
            if pulse > my_slices.zeros_threshold:
                my_slices.symbols.append(0)
                my_slices.symbols.append(0)
                low_length_long.append(pulse)
            else:
                my_slices.symbols.append(0)
                low_length_short.append(pulse)
            high = True
    # These convert the samples into microseconds --- based on workinf samp_rate

    my_slices.ones_long = np.average(high_length_long)*(1000000/my_slices.samp_rate)
    my_slices.ones_short = np.average(high_length_short)*(1000000/my_slices.samp_rate)
    my_slices.zeros_long = np.average(low_length_long)*(1000000/my_slices.samp_rate)
    my_slices.zeros_short = np.average(low_length_short)*(1000000/my_slices.samp_rate)
    
    return my_slices
    

"""
Embedded Python Blocks:

Each time this file is saved, GRC will instantiate the first class it finds
to get ports and parameters of your block. The arguments to __init__  will
be the parameters. All of them are required to have default values!
"""

import numpy as np
from gnuradio import gr
#import basic_help.py

# class blk(gr.sync_block):  # other base classes are basic_block, decim_block, interp_block
class blk(gr.basic_block):
    """Embedded Python Block example - a simple multiply const"""

    def __init__(self, working_samp_rate=400e3):  # only default arguments here
        """arguments to this function show up as parameters in GRC"""
        gr.basic_block.__init__(
            self,
            name='print symbols',   # will show up in GRC
            in_sig=[np.float32],
            out_sig=[]
        )
        # if an attribute with the same name as a parameter is found,
        # a callback is registered (properties work, too).
        self.working_samp_rate = working_samp_rate
        # self.filename = 'D:\OneDrives\OneDrive - Naval Postgraduate School\SDRLabs\sdr-labs-robodojo\ASK_Lab\data.out'
        self.sample_holder = np.array([],dtype='float32')
        self.slicer = Slices(samp_rate=working_samp_rate)
        self.logfile = 'D:\OneDrives\OneDrive - Naval Postgraduate School\SDRLabs\sdr-labs-robodojo\ASK_Lab\flowgraph_data.log'
        self.l = AsyncWrite(self.logfile)
        # experimentally determined value for genie  garage door opener
        # used to seperate bursts
        self.dist_between_bursts = .03*working_samp_rate

        

    # def work(self, input_items, output_items):
        # in a sync block, this was "work()", but the basic_block requires "general work"
    def general_work(self, input_items, output_items):
        """example: multiply with constant"""
        #output_items[0][:] = input_items[0] * self.example_param
        self.sample_holder = np.append(self.sample_holder,input_items[0])
        # would this be one second of data?
        if self.sample_holder.size > self.working_samp_rate*2:
            gathered_log = ('\n \t -------start check ----------------\n')
            gathered_log = gathered_log + ('checking burst with length '+ str(self.sample_holder.size))
            #self.l('checking with length '+ str(self.sample_holder.size))
            this_sample = np.array(self.sample_holder)

            # the complex data is converted into magnitudes
            mags = np.absolute(this_sample)

            # the threshold is automatically calculated by finding the maximum magnitude, then dividing it by two
            # this assumes no "noise" was included directly in this filtered data
            mythresh = np.amax(mags)/2

            # numpy creates an array of positions where the magnitudes are higher than the threshold value
            # this accomplishes nearly the same thing as gnuradio binary slicer, a kind of metadata though
            sliced_data = np.array(np.where(mags > mythresh )[0])

            # the differences between each consecutive number is calculated and placed in this new array
            new = np.array(np.ediff1d(sliced_data))

            # if the differences between the sliced data postions are greater than the average distance between the normal bursts,
            # then the number indicates a new burst, and this array holds the starting location of each noew burst.
            burst_segments = np.where(new > self.dist_between_bursts )[0]

            # chunk through and slice out each burst to analyze seperately
            cur = 0
            the_bursts = []
            for nex in burst_segments:
                # print(cur,nex)
                #print(sliced_data[cur],sliced_data[nex])
                # my_burst.show_spec() .001
                the_bursts.append(np.absolute(this_sample[sliced_data[cur]:sliced_data[nex]]))
                # shift over
                cur = nex+1
            
            # detect the gap between the preamble and data, using same method as above, but on single burst
            # based on specgram, looks like near .001 seconds, anything smaller should be treated as gap between data symbols
            gap_after_preamble = .001*self.working_samp_rate

            # now generate symbols for all bursts in this sample
            for burst in the_bursts:
                # the complex data is converted into magnitudes
                mags = np.absolute(burst)

                # numpy creates an array of positions where the magnitudes are higher than the threshold value
                # this accomplishes nearly the same thing as gnuradio binary slicer, a kind of metadata though
                sliced_data = np.array(np.where(mags > mythresh )[0])

                # the differences between each consecutive number is calculated and placed in this new array
                new = np.array(np.ediff1d(sliced_data))
    ### Work on the preamble
                # if the differences between the sliced data postions are greater than the average distance between the normal bursts,
                # then the number indicates a new burst, and this array holds the starting location of each noew burst.
                preamble_loc = np.where(new > gap_after_preamble )[0]

                # this slice is the preamble: (sliced_data[0],sliced_data[preamble_loc[0]])
                # and this is the base  (sliced_data[preamble_loc[0]+1],the_bursts[0].size)
                the_burst_pramble = np.absolute(burst[sliced_data[0]:sliced_data[preamble_loc[0]]])
                the_burst_base = np.absolute(burst[sliced_data[preamble_loc[0]+1]:burst.size])

                ## now to find widths of minimum symbols --- need to have an idea of minimum sumbol length in samples
                # zooming in on plot of preamble might help -- choose something like half the preamble bits
                # they look about 75 samples wide so...for samp_rate of 4e5, that is 187 microseconds (.0001875 seconds)
                # half that is 
                min_symbol_length = int(.00008125 * self.working_samp_rate)

                preamble_threshold  = np.amax(the_burst_pramble)/2

                #these are the locations where the magnitudes reach the threshold
                preamble_bit_locs = np.array(np.where(the_burst_pramble > preamble_threshold )[0])     

                # opposite list for gaps
                preamble_gap_locs = np.array(np.where(the_burst_pramble < preamble_threshold )[0])  

                # now where the difference between locations is only one, that is part of a consecutive bit, where it is more than one, that is a gap
                # this returns an array of th edifferences in consecutive values
                preamb_thresh_bit_diffs = np.diff(preamble_bit_locs)
                # same for gaps
                preamb_thresh_gap_diffs = np.diff(preamble_gap_locs)

                # this will return an array of where those values are more than our min_symbol length
                preamb_bit_locations = np.where(preamb_thresh_bit_diffs > min_symbol_length)

                #now get the gaps
                preamb_gap_locations = np.where(preamb_thresh_gap_diffs > min_symbol_length)
                # remember that the first location is the beginning of a bit

                ## correcting info lost from the diffs, adding the next bit/gap locations
                #  appending a value that is one more than the previous last value:
                preamb_bit_locations = np.append(preamb_bit_locations,(preamb_bit_locations[0][len(preamb_bit_locations[0])-1])+1)
                preamb_gap_locations = np.append(preamb_gap_locations,(preamb_gap_locations[0][len(preamb_gap_locations[0])-1])+1)

                #and this shows us the "lengths" between bits
                preamb_bit_lengths = np.diff(preamb_bit_locations)
                preamb_gap_lengths = np.diff(preamb_gap_locations)   

                # now combine the lists of locations of gaps and bits, and put them in order
                symbol_locs = np.array([])
                bit_list = []
                gap_list = []

                for locs in preamb_bit_locations:
                    val = preamble_bit_locs[locs]
                    symbol_locs = np.append(symbol_locs,val)
                    bit_list.append(val)
                    
                for locs in preamb_gap_locations:
                    val = preamble_gap_locs[locs]
                    symbol_locs = np.append(symbol_locs,val)
                    gap_list.append(val)

                symbol_list = np.sort(symbol_locs)
                print(symbol_list)
                # presumt the symbols start with a 1?
                symbol_vals = []
                # iterate through the_burst_base, testing the lengths/values of the symbols and assigning symbols
                for pos in symbol_list:
                    test = np.in1d(bit_list,pos)    #test = np.isin(bit_list,pos)
                    if len(np.where(test == True)[0]):
                        symbol_vals.append(1)
                    test = np.in1d(gap_list,pos)    #test = np.isin(gap_list,pos)
                    if len(np.where(test == True)[0]):
                        symbol_vals.append(0)           
    #### Calculating the burst base
                #now to evaluate the remainder of a burst
                # copying the methodology from above for the preamble

                min_symbol_length = int(.00008125 * self.working_samp_rate)
        
                # these are the magnitudes of the preamble part of the burst
                the_burst_base = np.absolute(the_bursts[0][sliced_data[preamble_loc[0]+1]:the_bursts[0].size])

                base_threshold  = np.amax(the_burst_base)/2

                #these are the locations where the magnitudes reach the threshold
                base_bit_locs = np.array(np.where(the_burst_base > base_threshold )[0])
                # oposite list for gaps
                base_gap_locs = np.array(np.where(the_burst_base < base_threshold )[0])

                # now where the difference between locations is only one, that is part of a consecutive bit, where it is more than one, that is a gap
                # this returns an array of th edifferences in consecutive values
                base_thresh_bit_diffs = np.diff(base_bit_locs)
                print("base_thresh_bit_diffs",base_thresh_bit_diffs)

                # same for gaps
                base_thresh_gap_diffs = np.diff(base_gap_locs)
                #   base_bit_locs[base_thresh_gap_diffs[base_bit_locations]] == location in the burst base
                # this will return an array of where those values are more than our min_symbol length
                base_bit_locations = np.where(base_thresh_bit_diffs > min_symbol_length)
                #now get the gaps
                base_gap_locations = np.where(base_thresh_gap_diffs > min_symbol_length)
                # remember that the first location is the beginning of a bit

                ## correcting info lost from the diffs, adding the next bit/gap locations
                #  appending a value that is one more than the previous last value:
                base_bit_locations = np.append(base_bit_locations,(base_bit_locations[0][len(base_bit_locations[0])-1])+1)
                base_gap_locations = np.append(base_gap_locations,(base_gap_locations[0][len(base_gap_locations[0])-1])+1)

                #and this shows us the "lengths" between bits
                base_bit_lengths = np.diff(base_bit_locations)
                base_gap_lengths = np.diff(base_gap_locations)       

                # all the bitwidths look very normal -- 
                # can now populate threshold values for bits - to determine when assigning 11 vs 1
                ones_threshold = np.average(base_bit_lengths)
                zeros_threshold = np.average(base_gap_lengths)                                                 

                # now combine the lists of locations of gaps and bits, and put them in order
                symbol_locs = np.array([])
                bit_list = []
                gap_list = []

                for locs in base_bit_locations:
                    val = base_bit_locs[locs]
                    symbol_locs = np.append(symbol_locs,val)
                    bit_list.append(val)
                    
                for locs in base_gap_locations:
                    val = base_gap_locs[locs]
                    symbol_locs = np.append(symbol_locs,val)
                    gap_list.append(val)

                symbol_list = np.sort(symbol_locs)
                ##print(symbol_list)
                # putting gap in between preamble and rest of data
                symbol_vals = symbol_vals + [0,0,0,0,0,0,0,0,0,0]
                prev = 0
                # iterate through the_burst_base, testing the lengths/values of the symbols and assigning symbols
                for pos in symbol_list:
                    test = np.in1d(bit_list,pos)    # test = np.isin(bit_list,pos)
                    if len(np.where(test == True)[0]):
                        if pos-prev > ones_threshold:
                            symbol_vals.append(1)
                            symbol_vals.append(1)
                        else:
                            symbol_vals.append(1)
                        prev = pos
                        continue
                    test = np.in1d(gap_list,pos)    #test = np.isin(gap_list,pos)
                    if len(np.where(test == True)[0]):
                        if pos-prev > zeros_threshold:
                            symbol_vals.append(0)
                            symbol_vals.append(0)
                        else:
                            symbol_vals.append(0)
                        prev = pos
                # for good measure....probably lost at least one 1, if not two. need a way to check it
                symbol_vals = symbol_vals + [1]
                gathered_log = gathered_log + (str(symbol_vals) + '\n')
                self.l.log(gathered_log)
                gathered_log = ''

            gathered_log = gathered_log + (str(symbol_vals))

            gathered_log = gathered_log +('\n \t -----------end check------------\n')
            self.l.log(gathered_log)

            # clear out this variable for next time
            self.sample_holder = np.array([],dtype='float32')

        return 0#len(output_items[0])