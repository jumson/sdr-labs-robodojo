#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Top Block
# Generated: Sat Oct 27 20:28:16 2018
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt4 import Qt
from gnuradio import blocks
from gnuradio import digital
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio import qtgui
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import osmosdr
import sip
import sys
import time
from gnuradio import qtgui


class top_block(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Top Block")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Top Block")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "top_block")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())

        ##################################################
        # Variables
        ##################################################
        self.working_samp_rate = working_samp_rate = 400000
        self.target_freq = target_freq = 433.943e6
        self.samp_rate = samp_rate = 2e6
        self.offset = offset = 200e3
        self.threshold = threshold = .001
        self.freq = freq = target_freq-offset
        self.filter_transition = filter_transition = 10e3
        self.filter_decimation = filter_decimation = int(samp_rate/working_samp_rate)
        self.filter_cutoff = filter_cutoff = .10e6
        self.filename = filename = "D:\OneDrives\OneDrive - Naval Postgraduate School\\SDRLabs\\sdr-samples\\SolidRemote\\solidremoteD_CF_433.943M_400k"
        self.bandwidth = bandwidth = 400e3

        ##################################################
        # Blocks
        ##################################################
        self._offset_tool_bar = Qt.QToolBar(self)
        self._offset_tool_bar.addWidget(Qt.QLabel("offset"+": "))
        self._offset_line_edit = Qt.QLineEdit(str(self.offset))
        self._offset_tool_bar.addWidget(self._offset_line_edit)
        self._offset_line_edit.returnPressed.connect(
        	lambda: self.set_offset(eng_notation.str_to_num(str(self._offset_line_edit.text().toAscii()))))
        self.top_grid_layout.addWidget(self._offset_tool_bar)
        self._target_freq_tool_bar = Qt.QToolBar(self)
        self._target_freq_tool_bar.addWidget(Qt.QLabel("target_freq"+": "))
        self._target_freq_line_edit = Qt.QLineEdit(str(self.target_freq))
        self._target_freq_tool_bar.addWidget(self._target_freq_line_edit)
        self._target_freq_line_edit.returnPressed.connect(
        	lambda: self.set_target_freq(eng_notation.str_to_num(str(self._target_freq_line_edit.text().toAscii()))))
        self.top_grid_layout.addWidget(self._target_freq_tool_bar)
        self.qtgui_sink_x_0_0 = qtgui.sink_c(
        	1024*2*2*2*2, #fftsize
        	firdes.WIN_BLACKMAN_hARRIS, #wintype
        	freq, #fc
        	samp_rate, #bw
        	"zero offset", #name
        	True, #plotfreq
        	True, #plotwaterfall
        	True, #plottime
        	True, #plotconst
        )
        self.qtgui_sink_x_0_0.set_update_time(1.0/10)
        self._qtgui_sink_x_0_0_win = sip.wrapinstance(self.qtgui_sink_x_0_0.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_sink_x_0_0_win)

        self.qtgui_sink_x_0_0.enable_rf_freq(False)



        self.multiply_constant = blocks.multiply_const_vcc((4, ))
        self.freq_xlating_fir_filter = filter.freq_xlating_fir_filter_ccc(filter_decimation, (firdes.low_pass(1, samp_rate, filter_cutoff, filter_transition)), offset, samp_rate)
        self.file_sink = blocks.file_sink(gr.sizeof_gr_complex*1, filename, False)
        self.file_sink.set_unbuffered(False)
        self.digital_binary_slicer = digital.binary_slicer_fb()
        self.complex_to_mag_squared = blocks.complex_to_mag_squared(1)
        self.blocks_uchar_to_float_0 = blocks.uchar_to_float()
        self.blocks_null_sink_0 = blocks.null_sink(gr.sizeof_float*1)
        self.add_const = blocks.add_const_vff((-1*threshold, ))
        self.HackRF_source_0 = osmosdr.source( args="numchan=" + str(1) + " " + "soapy=2,driver=hackrf" )
        self.HackRF_source_0.set_sample_rate(samp_rate)
        self.HackRF_source_0.set_center_freq(freq, 0)
        self.HackRF_source_0.set_freq_corr(0, 0)
        self.HackRF_source_0.set_dc_offset_mode(0, 0)
        self.HackRF_source_0.set_iq_balance_mode(0, 0)
        self.HackRF_source_0.set_gain_mode(False, 0)
        self.HackRF_source_0.set_gain(0, 0)
        self.HackRF_source_0.set_if_gain(20, 0)
        self.HackRF_source_0.set_bb_gain(20, 0)
        self.HackRF_source_0.set_antenna('', 0)
        self.HackRF_source_0.set_bandwidth(bandwidth, 0)


        ##################################################
        # Connections
        ##################################################
        self.connect((self.HackRF_source_0, 0), (self.freq_xlating_fir_filter, 0))
        self.connect((self.add_const, 0), (self.digital_binary_slicer, 0))
        self.connect((self.blocks_uchar_to_float_0, 0), (self.blocks_null_sink_0, 0))
        self.connect((self.complex_to_mag_squared, 0), (self.add_const, 0))
        self.connect((self.digital_binary_slicer, 0), (self.blocks_uchar_to_float_0, 0))
        self.connect((self.freq_xlating_fir_filter, 0), (self.file_sink, 0))
        self.connect((self.freq_xlating_fir_filter, 0), (self.multiply_constant, 0))
        self.connect((self.freq_xlating_fir_filter, 0), (self.qtgui_sink_x_0_0, 0))
        self.connect((self.multiply_constant, 0), (self.complex_to_mag_squared, 0))

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "top_block")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_working_samp_rate(self):
        return self.working_samp_rate

    def set_working_samp_rate(self, working_samp_rate):
        self.working_samp_rate = working_samp_rate
        self.set_filter_decimation(int(self.samp_rate/self.working_samp_rate))

    def get_target_freq(self):
        return self.target_freq

    def set_target_freq(self, target_freq):
        self.target_freq = target_freq
        self.set_freq(self.target_freq-self.offset)
        Qt.QMetaObject.invokeMethod(self._target_freq_line_edit, "setText", Qt.Q_ARG("QString", eng_notation.num_to_str(self.target_freq)))

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.set_filter_decimation(int(self.samp_rate/self.working_samp_rate))
        self.qtgui_sink_x_0_0.set_frequency_range(self.freq, self.samp_rate)
        self.freq_xlating_fir_filter.set_taps((firdes.low_pass(1, self.samp_rate, self.filter_cutoff, self.filter_transition)))
        self.HackRF_source_0.set_sample_rate(self.samp_rate)

    def get_offset(self):
        return self.offset

    def set_offset(self, offset):
        self.offset = offset
        Qt.QMetaObject.invokeMethod(self._offset_line_edit, "setText", Qt.Q_ARG("QString", eng_notation.num_to_str(self.offset)))
        self.set_freq(self.target_freq-self.offset)
        self.freq_xlating_fir_filter.set_center_freq(self.offset)

    def get_threshold(self):
        return self.threshold

    def set_threshold(self, threshold):
        self.threshold = threshold
        self.add_const.set_k((-1*self.threshold, ))

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq
        self.qtgui_sink_x_0_0.set_frequency_range(self.freq, self.samp_rate)
        self.HackRF_source_0.set_center_freq(self.freq, 0)

    def get_filter_transition(self):
        return self.filter_transition

    def set_filter_transition(self, filter_transition):
        self.filter_transition = filter_transition
        self.freq_xlating_fir_filter.set_taps((firdes.low_pass(1, self.samp_rate, self.filter_cutoff, self.filter_transition)))

    def get_filter_decimation(self):
        return self.filter_decimation

    def set_filter_decimation(self, filter_decimation):
        self.filter_decimation = filter_decimation

    def get_filter_cutoff(self):
        return self.filter_cutoff

    def set_filter_cutoff(self, filter_cutoff):
        self.filter_cutoff = filter_cutoff
        self.freq_xlating_fir_filter.set_taps((firdes.low_pass(1, self.samp_rate, self.filter_cutoff, self.filter_transition)))

    def get_filename(self):
        return self.filename

    def set_filename(self, filename):
        self.filename = filename
        self.file_sink.open(self.filename)

    def get_bandwidth(self):
        return self.bandwidth

    def set_bandwidth(self, bandwidth):
        self.bandwidth = bandwidth
        self.HackRF_source_0.set_bandwidth(self.bandwidth, 0)


def main(top_block_cls=top_block, options=None):

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
